import { useCallback, useMemo, useState } from "react";
import "./App.css";

const App = () => {
  const [userInput, setUserInput] = useState(0);

  function calculateFactorial(n) {
    let answer = 1;
    if (n === 0 || n === 1) {
      return answer;
    } else {
      for (var i = n; i >= 1; i--) {
        answer = answer * i;
      }
      return answer;
    }
  }

  const factorial = useMemo(() => {
    const result = calculateFactorial(userInput);
    return result;
  }, [userInput]);

  const handleChange = useCallback(() => {
    setUserInput(userInput + 1);
  }, [userInput]);

  return (
    <div className="App">
      <header className="App-header">
        <input type="number" value={userInput} onChange={handleChange} />
        <span>{factorial}</span>
      </header>
    </div>
  );
};

export default App;
